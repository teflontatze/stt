#pragma once


/** 
 *	This represents a view on a sequence of bytes. The data pointed to by @data is not guaranteed
 *	to be null-terminated. An invalid structure has @length 0 and @data is the nullpointer.
 *	You can check for this using the stt_str_is_valid macro.
 *	An empty String can be constructed using the stt_str_empty macro.
 **/

typedef struct string {
	unsigned 	length;
	char const 	*data;
} String;

/** Function like macros **/

// Checks, if @str is a valid String.
#define stt_str_is_valid(str) (str.data != NULL || str.length > 0)

// Constructs an empty String inline.
#define stt_str_empty() ((String){ .data = NULL, .length = 0 })

// Expands to formatspecifier for use in printf(). (Example: printf("%"PRISTR"\n", ARGSTR(str)))
#define PRIstr ".*s"
// Expands to appropriate arguments for use in printf().
#define PRIARGstr(str) (int)((str).length), ((str).data)

/** Constructors **/

// Constructs a String from a null terminated sequence of bytes.
String stt_str_from_cstr(char const *string);

// Constructs a String from a sequence of bytes that need not be null terminated.
String stt_str_from_bytes(char const *bytes, unsigned size);


/** "Instance"Methods **/
String stt_str_pop_first(String *self, char delim);


#ifdef LIBT_STRING_IMPL

String stt_str_from_cstr(char const *string)
{
	if (!string)
		return stt_str_empty();

	return stt_str_from_bytes(string, strlen(string));
}

String stt_str_from_bytes(char const *bytes, unsigned size)
{
	if (!bytes)
		return stt_str_empty();

	return (String){ .length = size, .data = bytes };
}


String stt_str_pop_first(String *self, char delim)
{
	if (!self)
		return stt_str_empty();

	String ret = stt_str_from_bytes(self->data, 0);

	while (self->length > 0 && *self->data != delim) {
		--self->length;
		++self->data;
		++ret.length;
	}

	if (self->length) {
		--self->length;
		++self->data;
	}

	return ret;
}

#endif /* LIBT_STRING_IMPL */
